﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cafe.Data.Models
{
    public class Employee
    {
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public int Salary { get; set; }
        public Function Function { get; set; }
        public DateTime EmploymentDate { get; set; }
    }
}
