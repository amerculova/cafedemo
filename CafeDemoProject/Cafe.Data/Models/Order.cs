﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cafe.Data.Models
{
    public class Order
    {
        public int OrderId { get; set; }
        public int Quantity { get; set; }
        public Employee Employee { get; set; }
        public Product Product { get; set; }
    }
}
