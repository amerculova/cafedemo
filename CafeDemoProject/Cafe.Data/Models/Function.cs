﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cafe.Data.Models
{
    public class Function
    {
        public int FunctionId { get; set; }
        public string Name { get; set; }
    }
}
