﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cafe.Data.Models
{
    public class Check
    {
        public int CheckId { get; set; }
        public Order Order { get; set; }
        public PaymentType PaymentType { get; set; }
        public DateTime Date { get; set; }
    }
}
