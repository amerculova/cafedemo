﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cafe.Data.Models
{
    public class PaymentType
    {
        public int PaymentTypeId { get; set; }
        public string Name { get; set; }
    }
}
