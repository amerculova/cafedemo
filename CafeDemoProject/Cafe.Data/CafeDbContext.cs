﻿using Cafe.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cafe.Data
{
    public class CafeDbContext : DbContext
    {
        public DbSet<Employee> employees { get; set; }
        public DbSet<Order> orders { get; set; }
        public DbSet<Check> checks { get; set; }
        public DbSet<Function> functions { get; set; }
        public DbSet<PaymentType> paymentTypes { get; set; }
        public DbSet<Product> products { get; set; }

        public CafeDbContext(DbContextOptions options)
             : base(options)
        {
        }
   
    }
}
